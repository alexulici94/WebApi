﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Contentful.Core;
using System.Net.Http;
using Contentful.Core.Models;
using System.Collections.Generic;
using Microsoft.AspNetCore.Cors;
using Microsoft.Extensions.Logging;
using ContentfulAPI.Contentful;

namespace ContentfulAPI.Controllers
{
    [EnableCors("MyPolicy")]
    [Route("api/[controller]")]
    public class ContentPageController : Controller
    {
        public readonly IContentfulService _contentfulService;

        public ContentPageController(IContentfulService contentfulService)
        {
            _contentfulService = contentfulService;
        }

        // GET api/values
        [HttpGet]
        public async Task<List<string>> Get()
        {
            var httpClient = new HttpClient();
            var client = new ContentfulClient(httpClient, "7a5d7f0b0fd78670002277a59b6d808cf85f7517b88994bbc23ff125c93ec3c2", "713245eba604cb3936a491968dad2f32152f25c36b1a79f87345f9bff04f455a", "uwg00q09c5xr");

            return await _contentfulService.GetAssets(client);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
