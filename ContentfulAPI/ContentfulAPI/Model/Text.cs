﻿namespace ContentfulAPI.Model
{
    public class Text
    {
        public string TextBox { get; set; }
        public string StarWarsImage { get; set; }
    }
}
