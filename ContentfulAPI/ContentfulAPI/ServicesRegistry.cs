﻿using ContentfulAPI.Contentful;
using StructureMap;

namespace ContentfulAPI
{
    internal class ServicesRegistry : Registry
    {
        public ServicesRegistry()
        {
            For<IContentfulService>().Use<ContentfulService>();
        }
    }
}