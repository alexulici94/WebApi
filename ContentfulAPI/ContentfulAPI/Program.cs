﻿using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace ContentfulAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = new WebHostBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseStartup<Startup>()
                .UseIISIntegration()
                .UseKestrel()
                .Build();

            host.Run();
        }
    }
}
