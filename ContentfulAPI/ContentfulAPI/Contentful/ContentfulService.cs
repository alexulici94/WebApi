﻿using Contentful.Core;
using Contentful.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ContentfulAPI.Contentful
{
    public class ContentfulService : IContentfulService
    {
        public async Task<List<string>> GetAssets(ContentfulClient client)
        {
            List<string> AssetsURL = new List<string>();
            ContentType contentType = await client.GetContentType("text");
            var entries = await client.GetEntries<dynamic>();
            foreach (var asset in entries.IncludedAssets)
            {
                AssetsURL.Add("https:" + asset.File.Url);
            }
            return AssetsURL;
        }
    }
}
