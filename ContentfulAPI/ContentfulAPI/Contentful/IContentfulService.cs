﻿using Contentful.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContentfulAPI.Contentful
{
    public interface IContentfulService
    {
        Task<List<string>> GetAssets(ContentfulClient client);
    }
}
