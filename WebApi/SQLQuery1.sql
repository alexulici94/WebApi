use webapi
CREATE TABLE Car
(
 Id int,
 Name varchar(20),
 Price DECIMAL(5,2),
 PRIMARY KEY (Id)
);