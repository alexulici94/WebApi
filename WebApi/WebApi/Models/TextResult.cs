﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace WebApi.Models
{
    public class TextResult:IHttpActionResult
    {
        public string value { get; }
        HttpRequestMessage _request;

        public TextResult(string value, HttpRequestMessage request)
        {
            this.value = value;
            _request = request;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var response = new HttpResponseMessage()
            {
                Content = new StringContent(value),
                RequestMessage = _request
            };
            return Task.FromResult(response);
        }

    }
}