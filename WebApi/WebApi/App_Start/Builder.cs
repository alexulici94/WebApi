﻿using Autofac;
using WebApi.Domain;
using WebApi.Repository;

namespace WebApi.App_Start
{
    public class Builder
    {
        public static IContainer Container { get; set; }
        public static void Build()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<CarDBRepository>().As<IRepository<Car>>();
            builder.RegisterType<CommentsDBRepository>().As<IRepository<Comment>>();
            Container = builder.Build();
        }
    }
}