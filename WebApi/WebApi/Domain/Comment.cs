﻿using System;

namespace WebApi.Domain
{
    public class Comment
    {
        public string Id { get; set; }
        public string Author { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
    }
}