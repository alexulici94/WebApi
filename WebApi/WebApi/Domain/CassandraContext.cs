﻿using Cassandra;
using System;
using System.Collections.Generic;

namespace WebApi.Domain
{
    public class CassandraContext
    {
        private ISession session;

        public CassandraContext()
        {
            Cluster cluster = Cluster.Builder().AddContactPoint("127.0.0.1").Build();
            session = cluster.Connect();
            //Create keyspace
            session.CreateKeyspaceIfNotExists("commentsspace");

            //Set active keyspace
            session.ChangeKeyspace("commentsspace");

            //Create comments table
            session.Execute("CREATE TABLE if not exists commentsspace.Comments (id varchar PRIMARY KEY, author varchar, text varchar, date varchar);");
        }

        public void AddComment(Comment comment)
        {
            var query = "INSERT INTO commentsspace.Comments (id, author, text, date) VALUES (?,?,?,?)";
            PreparedStatement preparedStatement = session.Prepare(query);
            BoundStatement boundStatement = preparedStatement.Bind(comment.Id, comment.Author,comment.Text,comment.Date.ToString());
            session.Execute(boundStatement);
        }

        public IEnumerable<Comment> GetAllComments()
        {
            List<Comment> commentsList = new List<Comment>();
            var comments = session.Execute("SELECT id, author, text, date FROM commentsspace.Comments");
            foreach(var row in comments.GetRows())
            {
                commentsList.Add(MapComment(row));
            }
            return commentsList;
        }

        private Comment MapComment(Row row)
        {
            Comment newComment = new Comment
            {
                Id = row["id"].ToString(),
                Date = Convert.ToDateTime(row["date"]),
                Text = row["text"].ToString(),
                Author = row["author"].ToString()
            };
            return newComment;
        }
    }
}