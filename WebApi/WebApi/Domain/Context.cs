﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebApi.Domain
{
    public class Context : DbContext
    {
        public DbSet<Car> Cars { get; set; }
    }
}