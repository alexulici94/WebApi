﻿using Autofac;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Results;
using WebApi.App_Start;
using WebApi.Domain;
using WebApi.Repository;

namespace WebApi.Controllers
{
    [EnableCors("*", "*", "*")]
    public class CommentsController : ApiController
    {
        private readonly IContainer Container;
        private IRepository<Comment> repository;

        public CommentsController()
        {
            Container = Builder.Container;
        }

        public CommentsController(IRepository<Comment> repository)
        {
            Container = Builder.Container;
            this.repository = repository;
        }

        [HttpPost]
        public void AddCar([FromBody] Comment comment)
        {
            var writer = GetWriter<IRepository<Comment>>();
            writer.Add(comment);
        }

        [HttpGet]
        public JsonResult<IEnumerable<Comment>> GetComments()
        {
            var writer = GetWriter<IRepository<Comment>>();
            return Json(writer.GetAll());
        }

        private T GetWriter<T>()
        {
            var scope = Container.BeginLifetimeScope();
            var writer = scope.Resolve<T>();
            return writer;
        }
    }
}