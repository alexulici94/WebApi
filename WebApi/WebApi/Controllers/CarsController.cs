﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Autofac;
using WebApi.App_Start;
using WebApi.Domain;
using WebApi.Models;
using WebApi.Repository;
using System.Web.Http.Results;
using System.Web.Http.Cors;

namespace WebApi.Controllers
{
    [EnableCors("*", "*", "*")]
    public class CarsController : ApiController
    {
        private readonly IContainer Container;
        private IRepository<Car> @object;

        public CarsController()
        {
            Container = Builder.Container;
        }

        public CarsController(IRepository<Car> @object)
        {
            Container = Builder.Container;
            this.@object = @object;
        }

        [HttpGet]
        public JsonResult<IEnumerable<Car>> GetCars()
        {
            var writer = GetWriter<IRepository<Car>>();
            return Json(writer.GetAll());
        }

        [HttpGet]
        public IHttpActionResult GetCar(int id)
        {
            var writer = GetWriter<IRepository<Car>>();
            return (from c in writer.GetAll() where c.Id.Equals(id) select Ok(c)).FirstOrDefault();
        }

        [HttpPut]
        public void UpdateCar(Car car)
        {
            var writer = GetWriter<IRepository<Car>>();
            writer.Update(car);
        }

        [HttpGet]
        public IHttpActionResult HelloCar(string name)
        {
            return new TextResult("Hello " + name, Request);
        }

        [HttpGet]
        public List<Car> SortBy(string type)
        {
            var writer = GetWriter<IRepository<Car>>();
            var pinfo = typeof(Car).GetProperty(type);
            return pinfo == null ? null : writer.GetAll().OrderBy(o => pinfo.GetValue(o, null)).ToList();
        }

        [HttpPost]
        public void AddCar([FromBody] Car c)
        {
            var writer = GetWriter<IRepository<Car>>();
            writer.Add(c);
        }

        [HttpDelete]
        public void RemoveCar([FromBody] Car c)
        {
            var writer = GetWriter<IRepository<Car>>();
            writer.Remove(c);
        }

        private T GetWriter<T>()
        {
            var scope = Container.BeginLifetimeScope();
            var writer = scope.Resolve<T>();
            return writer;
        }
    }
}