﻿using System;
using System.Collections.Generic;
using WebApi.Domain;

namespace WebApi.Repository
{
    public class CarDBRepository : IRepository<Car>
    {
        Context context = new Context();

        public void Add(Car entity)
        {
            context.Cars.Add(entity);
            context.SaveChanges();
        }

        public IEnumerable<Car> GetAll()
        {
            return new List<Car>(context.Set<Car>());
        }

        public Car GetEntity(int id)
        {
            return context.Cars.Find(id);
        }

        public void Remove(Car entity)
        {
            context.Cars.Attach(entity);
            context.Cars.Remove(entity);
            context.SaveChanges();
        }

        public void Update(Car entity)
        {
            var result = context.Cars.Find(entity.Id);
            result.Name = entity.Name;
            result.Price = entity.Price;
            result.Color = entity.Color;
            context.SaveChanges();
        }
    }
}