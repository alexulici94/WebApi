﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Models;
using WebApi.Repository;

namespace WebApi.Repository
{
    public class ListRepository<T> : IRepository<T> where T : class
    {
        List<T> list = new List<T>();

        public void Add(T entity)
        {
            list.Add(entity);
        }

        public IEnumerable<T> GetAll()
        {
            return list;
        }

        public T GetEntity(int pos)
        {
            return list.ElementAt(pos);
        }

        public void Remove(T entity)
        {
            list.Remove(entity);
        }

        public void Update(T entity)
        {
        }
    }
}