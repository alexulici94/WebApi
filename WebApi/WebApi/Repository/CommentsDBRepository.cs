﻿using System;
using System.Collections.Generic;
using WebApi.Domain;

namespace WebApi.Repository
{
    public class CommentsDBRepository : IRepository<Comment>
    {
        CassandraContext cassandraContext;

        public CommentsDBRepository()
        {
            cassandraContext = new CassandraContext();
        }

        public void Add(Comment entity)
        {
            entity.Id = Guid.NewGuid().ToString();
            entity.Date = DateTime.Now;
            cassandraContext.AddComment(entity);
        }

        public IEnumerable<Comment> GetAll()
        {
            return cassandraContext.GetAllComments();
        }

        public Comment GetEntity(int pos)
        {
            throw new NotImplementedException();
        }

        public void Remove(Comment entity)
        {
            throw new NotImplementedException();
        }

        public void Update(Comment entity)
        {
            throw new NotImplementedException();
        }
    }
}