﻿using Ploeh.AutoFixture;
using Ploeh.AutoFixture.AutoMoq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Controllers;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.QualityTools.UnitTestFramework;
using Moq;
using WebApi.Repository;
using WebApi.Models;
using System.Data.Entity.Infrastructure;
using System.Data.Entity;
using WebApi.App_Start;
using System.Web.Http;
using System.Web.Http.Results;
using WebApi.Domain;

namespace WebApi.Unit_Tests
{
    [TestClass]
    public class CarsControllerTests
    {
        private IFixture fixture;
        private Mock<IRepository<Car>> mockRepository;
        private CarsController sut;

        [TestInitialize]
        public void Initialize()
        {
            Builder.Build();
            fixture = new Fixture().Customize(new AutoMoqCustomization());
            mockRepository = new Mock<IRepository<Car>>();
            sut = new CarsController(mockRepository.Object);
        }

        [TestCleanup]
        public void Cleanup()
        {
            
        }

        [TestMethod]
        public void AddCar_Added_True()
        {
            var car = fixture.Build<Car>().Create();
            car.Id = FindFreeId();
            car.Name = car.Name.Substring(5, 5);
            car.Color = car.Color.Substring(5, 5);
            mockRepository.Setup(e => e.Add(It.IsAny<Car>()));
            int size = 2;
            sut.AddCar(car);
            int newsize = 2;

            Assert.AreEqual<int>(size+1, newsize);
        }

        //[TestMethod]
        //public void AddCar_AlreadyExists_ExceptionThrown()
        //{
        //    var car = fixture.Create<Car>();
        //    car.Id = FindFreeId();
        //    car.Name = car.Name.Substring(5, 5);
        //    car.Color = car.Color.Substring(5, 5);

        //    sut.AddCar(car);
        //    Action ac = () => sut.AddCar(car);

        //    ac.ShouldThrow<DbUpdateException>();
        //}

        [TestMethod]
        public void RemoveCar_Removed_True()
        {
            var car = fixture.Create<Car>();
            var size = 2;
            car.Id = FindFreeId();
            car.Name = car.Name.Substring(5, 5);
            car.Color = car.Color.Substring(5, 5);

            sut.AddCar(car);
            sut.RemoveCar(car);
            var newsize =2;

            Assert.AreEqual<int>(size, newsize);
        }

        [TestMethod]
        public void RemoveCar_DoesntExist_ExceptionThrown()
        {
            var car = fixture.Create<Car>();
            car.Id = FindFreeId();
            car.Name = "test3";

            Action ac = () => sut.RemoveCar(car);

            ac.ShouldThrow<DbUpdateException>();
        }

        [TestMethod]
        public void SortBy_Correctness_True()
        {
            bool fail=false;

            var l = sut.SortBy("Id");
            for(int i=0;i<l.Count-1;i++)
            {
                if (l[i].Id > l[i + 1].Id)
                    fail = true;
            }

            fail.Should().BeFalse();
        }

        [TestMethod]
        public void SortBy_UnwantedParameter_ExceptionThrown()
        {
            var str = fixture.Create<string>();

            var l = sut.SortBy(str);

            l.Should().BeNull();
        }

        [TestMethod]
        public void HelloCar_Correctness_True()
        {
            var str = fixture.Create<string>();

            var action = sut.HelloCar(str);
            var result = action as TextResult;

            Assert.AreEqual("Hello " + str, result?.value);
        }

        private int FindFreeId()
        {
            var i = 0;
            while(i != -1)
            {
                if (sut?.GetCar(i) != null)
                    i++;
                else
                    return i;
                if (i <= 10000) continue;
                throw new ArgumentException("Couldn't find a free ID");
            }
            return -1;
        }
    }
}