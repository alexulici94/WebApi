import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import * as constants from './components/Constants';
import { Route, Router, Switch } from 'react-router-dom';
//Auth
import Auth from './components/auth/Auth';
import Callback from './components/auth/Callback';
import history from './components/auth/History';
//Apollo
import { ApolloProvider } from 'react-apollo';
import { HttpLink } from 'apollo-link-http';
import { split } from 'apollo-link';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloClient } from 'apollo-client';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';
//Components
import App from './components/App';
import Home from './views/HomeView';
import Comments from './views/CommentsView';
import Contentful from './views/ContentfulView';
import Posts from './views/PostsView';
import PostPage from './components/posts/PostPage';
import AddPost from './components/posts/AddPost';

const wsLink = new WebSocketLink({
  uri: constants.GRAPHQL_SUBSCRIBE_ADRESS,
  options: {
    reconnect: true
  }
});

const httpLink = new HttpLink({
  uri: constants.GRAPHQL_SERVER_ADRESS
});

const link = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query);
    return kind === 'OperationDefinition' && operation === 'subscription';
  },
  wsLink,
  httpLink,
);

const client = new ApolloClient({
  link: link,
  cache: new InMemoryCache().restore(window.__APOLLO_STATE__),
});

const auth = new Auth();

ReactDOM.render((
  <ApolloProvider client={client}>
    <Router history={history} component={App}>
      <div>
        <Route path={constants.APP_ROUTE} render={(props) => <App auth={auth} {...props} />} />
        <Route path={constants.HOME_ROUTE} render={(props) => <Home auth={auth} {...props} />} />
        <Route path={constants.COMMENTS_ROUTE} render={(props) => <Comments auth={auth} {...props} />} />
        <Route path={constants.CONTENTFUL_ROUTE} render={(props) => <Contentful auth={auth} {...props} />} />
        <Switch>
          <Route path={constants.ADD_POST_ROUTE} render={(props) => <AddPost auth={auth} {...props} />} />
          <Route path={constants.POST_ROUTE} render={(props) => <PostPage auth={auth} {...props} />} />
          <Route path={constants.POSTS_ROUTE} render={(props) => <Posts auth={auth} {...props} />} />
        </Switch>
        <Route path={constants.CALLBACK_ROUTE} render={(props) => <Callback auth={auth} {...props} />
        } />
      </div>
    </Router>
  </ApolloProvider>
),
  document.getElementById('root')
)
registerServiceWorker();
