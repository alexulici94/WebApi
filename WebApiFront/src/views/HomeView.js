import React, { Component } from 'react';
import '../css/Home.css';
import Search from '../components/home/SearchBox';
import Add from '../components/home/AddEntry';
import Products from '../components/home/ProductsList';
import Modal from '../components/home/EntryModal';
import { Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import * as constants from '../components/Constants';

class HomeView extends Component {
  state = { searchValue: '', edit: 'false', selectedElement: '', features: [] }

  componentWillMount() {
    fetch(constants.CONSUL_SERVER_ADRESS + constants.CONSUL_FEATURES_KEY + "?raw")
      .then(function (result) {
        if (result.ok) {
          return result.json()
        }
        throw new Error(constants.NETWORK_RESPONSE_FAILED);
      })
      .then(data => {
        this.setState({ features: data, isLoading: false })
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  onSearchChange = (event) => {
    this.setState({ searchValue: event.target.value });
  }

  onModalOpen = (target, isEdit) => {
    if (!this.modal)
      return;
    this.modal.openModal();
    this.setState({ edit: isEdit, selectedElement: target });
  }

  onEntityChange = (entity, action) => {
    this.products.onProductChange(entity, action);
  }


  render() {
    const { isAuthenticated } = this.props.auth;

    return (
      <Col xs={6} md={6} className="body">
        <Col xs={2} md={2} className="search">
          <Search onChangeValue={this.onSearchChange} />
        </Col>
        {isAuthenticated() ? <Add onButtonClick={this.onModalOpen} /> : null}
        <Products onRowClick={this.onModalOpen} filterValue={this.state.searchValue} onRef={ref => this.products = ref} />
        {isAuthenticated() ? <Modal onRef={ref => this.modal = ref} editFlag={this.state.edit} target={this.state.selectedElement} onEdit={this.onEntityChange} /> : null}
        {this.state.features[constants.FEATURE_COMMENTS] === true ? <Link to={constants.COMMENTS_ROUTE}><Button bsStyle="primary">{constants.BUTTON_COMMENTS}</Button></Link> : null}
        {this.state.features[constants.FEATURE_CONTENTFUL] === true ? <Link to={constants.CONTENTFUL_ROUTE}><Button bsStyle="primary">{constants.BUTTON_CONTENTFUL}</Button></Link> : null}
        {this.state.features[constants.FEATURE_POSTS] === true ? <Link to={constants.POSTS_ROUTE}><Button bsStyle="primary">{constants.BUTTON_POSTS}</Button></Link> : null}
      </Col>
    );
  }
}

export default HomeView;
