import React, { Component } from 'react';
import PostsList from '../components/posts/PostsList';
import { Button, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import '../css/Comments.css';
import * as constants from '../components/Constants';

class PostsView extends Component {

  onNewComment = (comment) => {
    this.commentsList.addNewComment(comment);
  }

  render() {
    return (
      <div>
        <Col xs={6} md={6}>
          <Link to={constants.HOME_ROUTE}><Button bsStyle="primary">{constants.BUTTON_BACK}</Button></Link>
          <PostsList auth={this.props.auth} onRef={ref => this.commentsList = ref}/>
        </Col>
        <Col xs={6} md={6}>
          {this.props.auth.isAuthenticated() ? <Link to={constants.ADD_POST_ROUTE}><Button bsStyle="primary">{constants.ACTION_ADD}</Button></Link> : null}
        </Col>
      </div>
    );
  }
}

export default PostsView;
