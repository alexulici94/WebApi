import React, { Component } from 'react';
import { Button, Image } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import * as constants from '../components/Constants';

class CommentsView extends Component {
    state = {pageContent: ''}

    componentWillMount()
    {
        fetch("http://" + constants.CONTENTFUL_API + "/api/contentpage")
        .then(result=>result.json())
        .then(data=>this.setState({ pageContent: data}));
    }
    
    render() {
        let content = null;
        if(this.state.pageContent)
        {
            content = this.state.pageContent.map((item, i) => (
                <Image key={i} src={item} />
            ));
        }

        return (
            <div>
                <Link to={constants.HOME_ROUTE}><Button bsStyle="primary">{constants.BUTTON_BACK}</Button></Link>
                <div>
                {content}
                </div>
            </div>
        );
    }
}

export default CommentsView;
