import React, { Component } from 'react';
import CommentBox from '../components/comments/CommentBox';
import CommentsList from '../components/comments/CommentsList';
import { Button, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import '../css/Comments.css';
import * as constants from '../components/Constants';

class CommentsView extends Component {

  onNewComment = (comment) => {
    this.commentsList.addNewComment(comment);
  }

  render() {
    const { isAuthenticated } = this.props.auth;

    return (
      <div>
        <Col xs={6} md={6} className="commentsBody">
          <Link to={constants.HOME_ROUTE}><Button bsStyle="primary">{constants.BUTTON_BACK}</Button></Link>
          {isAuthenticated() ? <CommentBox auth={this.props.auth} onNewComment={this.onNewComment}/> : null }
          <CommentsList onRef={ref => this.commentsList = ref}/>
        </Col>
      </div>
    );
  }
}

export default CommentsView;
