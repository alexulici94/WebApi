import React, { Component } from 'react';
import { ControlLabel, Glyphicon } from 'react-bootstrap';
import '../../css/Profile.css';

class Profile extends Component {
  state = { profile: {} };

  componentWillMount() {
    const profile = this.props.auth.getProfile();
    if(profile)
      this.setState({profile:profile});
  }

  render() {
    const { profile } = this.state;
    return (
      <div>
        <img className='img-responsive' src={profile.picture} alt="profile" />
        <div>
          <ControlLabel><Glyphicon glyph="user" /> {profile.name}</ControlLabel>
        </div>
      </div>
    );
  }
}

export default Profile;