import React from 'react';
import { Jumbotron, Label, Tooltip, OverlayTrigger, ControlLabel }  from 'react-bootstrap';
import '../../css/Comments.css';
import * as constants from '../Constants';
import Loading from '../Loading';

class CommentList extends React.Component {
	state = { comments: [], isLoading: true};

	componentDidMount()
    {
		this.props.onRef(this)
        this.getCommentsList();
	}

	componentWillUnmount() {
        this.props.onRef(null)
    }
	
	getCommentsList() {
        fetch("http://" + constants.CARS_API + "/api/comments")
			.then(function(result){
				if(result.ok) {
					return result.json()
				}
				throw new Error(constants.NETWORK_RESPONSE_FAILED);
			})
			.then(data=>this.setState({ comments: data, isLoading: false }))
			.catch(function(error) {
				console.log(error);
			});
	}

	addNewComment(comment) {
		var newList = this.state.comments.slice();    
		newList.push(comment);
		this.setState({comments:newList});
	}
	
	render(){
		var commentNodes = this.state.comments
			.sort((a, b) => a.Date > b.Date)
			.map(function(comment){
				return (
					<Jumbotron key={comment.Id}>
						<OverlayTrigger placement="right" overlay={<Tooltip id="tooltip">{comment.Date}</Tooltip>}>
							<Label>{comment.Author}</Label>
						</OverlayTrigger>
						<br />
						<ControlLabel>{comment.Text}</ControlLabel>
					</Jumbotron>
				);
			});
		return (
			<div>
				<Loading isLoading={this.state.isLoading} />
				<ul className='comment-list'>
					{commentNodes}
				</ul>
			</div>
		);
	}
}

export default CommentList;