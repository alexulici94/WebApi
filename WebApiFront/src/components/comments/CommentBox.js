import React, { Component } from 'react';
import { textarea, Button }  from 'react-bootstrap';
import * as constants from '../Constants';

class CommentBox extends Component {
    state = { textBox: ''}

    addNewComment = () => {
        const comment = {
            Id: '',
            Author: this.props.auth.getProfile().name,
            Text: this.state.textBox,
            Date: ''
        };
        fetch('http://' + constants.CARS_API + '/api/comments', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(comment)
        }).then(response => {
            if (response.ok) {
                this.props.onNewComment(comment);
                this.setState({textBox: ''})
            }
        });
    }

    onTextChange = (event) => {
        this.setState({textBox: event.target.value});
    }

    render() {
        return (
            <div>
            <textarea value={this.state.textBox} onChange={this.onTextChange}></textarea>
            <Button bsStyle="primary" onClick={this.addNewComment}>{constants.BUTTON_ADD}</Button>
            </div>
        );
      }
};

export default CommentBox;