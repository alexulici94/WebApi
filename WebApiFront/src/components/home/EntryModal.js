import React, { Component } from 'react';
import { ControlLabel, FormControl, Button, Modal }  from 'react-bootstrap';
import * as constants from '../Constants';

class EntryModal extends Component {
    state = {
        toggleModal: false,
        entry: {}
    };

    componentDidMount() {
        this.props.onRef(this)
      }
      componentWillUnmount() {
        this.props.onRef(null)
      }
    
    handleClose = (event) => {
        this.setState({
            toggleModal: false
        })
    }

    openModal()
    {
        this.setState({toggleModal: true})
    }

    addNewEntity = () =>{
        const car = {
            Id: 0,
            Name: this.name.value,
            Price: this.price.value,
            Color: this.color.value
        };
        fetch('http://' + constants.CARS_API + '/api/cars', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(car)
        }).then(response => {
            if (response.ok) {
                this.props.onEdit(car, constants.ACTION_ADD);
                this.handleClose();
            }
        });
    }

    editEntity = () =>{
        const car = {
            Id: this.props.target.Id,
            Name: this.name.value,
            Price: this.price.value,
            Color: this.color.value
        };
        fetch('http://'  + constants.CARS_API + '/api/cars', {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(car)
        }).then(response => {
            if (response.ok) {
                this.props.onEdit(car, constants.ACTION_EDIT);
                this.handleClose();
            }
        });
    }

    deleteEntity = () =>{
        const car = {
            Id: this.props.target.Id,
            Name: this.props.target.Name,
            Price: this.props.target.Price,
            Color: this.props.target.Color
        };
        fetch('http://'  + constants.CARS_API + '/api/cars', {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(car)
        }).then(response => {
            if (response.ok) {
                this.props.onEdit(car, constants.ACTION_DELETE);
                this.handleClose();
            }
        });
    }

    render() {
        return (
            <Modal show={this.state.toggleModal} onHide={this.handleClose}>
                <Modal.Header>
                    <Modal.Title>{this.props.editFlag ? constants.MODALTITLE_EDIT : constants.MODALTITLE_ADD }</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <ControlLabel>{constants.MODALBODY_INPUT2_LABEL}</ControlLabel>
                    <FormControl defaultValue={ this.props.editFlag ? this.props.target.Name : ''} inputRef={el => this.name = el} />

                    <ControlLabel>{constants.MODALBODY_INPUT3_LABEL}</ControlLabel>
                    <FormControl defaultValue={ this.props.editFlag ? this.props.target.Price : ''} inputRef={el => this.price = el} />

                    <ControlLabel>{constants.MODALBODY_INPUT4_LABEL}</ControlLabel>
                    <FormControl defaultValue={ this.props.editFlag ? this.props.target.Color : ''} inputRef={el => this.color = el} />
                </Modal.Body>

                <Modal.Footer>
                    <Button bsStyle="warning" onClick = {this.handleClose}>{constants.BUTTON_CLOSE}</Button>
                    {this.props.editFlag ? <Button bsStyle="danger" onClick={this.deleteEntity}>{constants.BUTTON_DELETE}</Button> : null }
                    {this.props.editFlag ? <Button bsStyle="success" onClick={this.editEntity}>{constants.BUTTON_EDIT}</Button> :
                    <Button bsStyle="primary" onClick={this.addNewEntity}>{constants.BUTTON_ADD}</Button> }
                    
                </Modal.Footer>
            </Modal>
        );
      }
};

export default EntryModal;