import React, { Component } from 'react';
import { Button, Glyphicon }  from 'react-bootstrap';

class AddEntry extends Component {
    render() {
        return (
            <Button bsStyle="primary" onClick={ this.props.onButtonClick }><Glyphicon glyph="plus"/></Button>
        );
      }
};

export default AddEntry;