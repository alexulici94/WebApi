import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import '../../css/Table.css'
import * as constants from '../Constants';
import Loading from '../Loading';
import { createStream } from 'redux-eventstore';

const stream = createStream(constants.EVENT_STORE_SERVER_ADRESS, constants.CARS_MANAGEMENT_STREAM);

class ProductsList extends Component {
    constructor(props) {
        super(props);
    
        this.state = {
            carsList: [],
            isLoading: true,
            filterList: [],
            filterValue: ''
        };
      }
    componentDidMount()
    {
        this.props.onRef(this)
        this.userList();
    }

    componentWillUnmount() {
        this.props.onRef(null)
    }

    componentWillReceiveProps(nextProps)
    {
        this.filterList(nextProps.filterValue);
    }

    onProductChange (product, action) {
        
        stream.write({ type: action, product: product });

        switch(action) {
            case constants.ACTION_ADD:
                this.addToList(product);
                break;
            case constants.ACTION_EDIT:
                this.editInList(product);
                break;
            case constants.ACTION_DELETE:
                this.removeFromList(product);
                break;
            default:
                return;
        }
        this.filterList(this.state.filterValue);
    }

    addToList(entity)
    {
        var newList = this.state.carsList.slice();    
        newList.push(entity);   
        this.setState({carsList:newList});
    }

    editInList(entity)
    {
        var newList = this.state.carsList.map( (item, index) => {
            if(item.Id !== entity.Id) {
                return item;
            }
            return entity;    
        });
        this.setState({carsList:newList});
    }

    removeFromList(entity)
    {
        this.setState({
            carsList: this.state.carsList.filter(i => i.Id !== entity.Id)
          });
    }

    filterList(value)
    {
        this.setState({filterValue:value});
        var updatedList = this.state.carsList;
        updatedList = updatedList.filter(function(item){
        return item.Name.toLowerCase().search(value.toLowerCase()) !== -1 ||
        item.Price.toString().toLowerCase().search(value.toLowerCase()) !== -1 ||
        item.Color.toString().toLowerCase().search(value.toLowerCase()) !== -1
        });
        this.setState({filterList: updatedList});
    }

    userList() {
        fetch("http://" + constants.CARS_API + "/api/cars")
            .then(result=>result.json())
            .then(data=>this.setState({ carsList: data, isLoading: false, filterList: data }));
    }

    render() {
        const cars = this.state.filterList.map((item, i) => (
                <tr key= {i} onClick={()=>this.props.onRowClick(item,'true')}>
                    <th key={item.id}>{ item.Name }</th>
                    <th key={item.id}>{ item.Price }</th>
                    <th key={item.id}>{ item.Color }</th>
                </tr>
        ));

        return (
            <div>
            <Table className="table" bordered hover>
                <thead>
                <tr>
                    <th>{constants.MODALBODY_INPUT2_LABEL}</th>
                    <th>{constants.MODALBODY_INPUT3_LABEL}</th>
                    <th>{constants.MODALBODY_INPUT4_LABEL}</th>
                </tr>
                </thead>
                <tbody>
                    {cars}
                </tbody>
            </Table>
            <Loading isLoading={this.state.isLoading} />
            </div>
        );
      }
};

export default ProductsList;