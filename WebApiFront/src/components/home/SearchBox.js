import React, { Component } from 'react';
import { FormControl } from 'react-bootstrap';
import '../../css/SearchBox.css'

class SearchBox extends Component {
    render() {
        return (
            <FormControl  type="text" onChange={this.props.onChangeValue} className="SelectBox"/>
        );
      }
};

export default SearchBox;