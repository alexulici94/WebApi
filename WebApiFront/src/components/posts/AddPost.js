import React, { Component } from 'react';
import { ControlLabel, Col, Image, Button, Glyphicon, FormControl } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import * as constants from '../Constants';
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import history from '../auth/History';
import PubSub from 'pubsub-js'

const NEWPOST_MUTATION = gql`
mutation NewPost($imageUrl:String!, $title:String!, $userId: ID!){
    createPost(imageUrl:$imageUrl,title:$title,userId:$userId){
      createdAt
      id
      imageUrl
      title
    }
  }
`

class AddPost extends Component {
    state = { imageUrl: '', isLoading: true };

    addNewPost = () => {
        this.props.NewPostMutation({
            variables: {
                title: this.title.value,
                imageUrl: this.state.imageUrl,
                userId: this.props.auth.getGraphCoolId()
            },
        }).then((result) => {
            history.replace(constants.POSTS_ROUTE)
            PubSub.publish(constants.SUBSCRIBE_ADD_POST, result);
        })
    }

    onChange = (event) => {
        this.setState({imageUrl: event.target.value});
      }

    render() {
        return (
            <div>
                <Col xs={6} md={6}>
                    <Link to={constants.POSTS_ROUTE}><Button bsStyle="primary">{constants.BUTTON_BACK}</Button></Link>
                    <br />
                    <ControlLabel>{constants.POST_TITLE}</ControlLabel>
                    <FormControl inputRef={el => this.title = el} />
                    <ControlLabel>{constants.POST_URL}</ControlLabel>
                    <FormControl value={this.state.imageUrl} onChange={this.onChange} />
                    <Button bsStyle="primary" onClick={ this.addNewPost }><Glyphicon glyph="plus"/></Button>
                    <br />
                    <Image src={this.state.imageUrl} />
                </Col>
            </div>
        );
    }
}

const mapPropsToOptions = props => ({ variables: { id: props.match.params.id } });

export default graphql(NEWPOST_MUTATION, { name: 'NewPostMutation', options: mapPropsToOptions })(AddPost)