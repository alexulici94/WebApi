import React from 'react';
import { Media, Col, Row, Grid, OverlayTrigger, Tooltip }  from 'react-bootstrap';
import '../../css/Comments.css';
import Loading from '../Loading';
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

const COMMENTS_QUERY = gql`
query GetCommentsForPost($filter: CommentFilter) {
	allComments(filter:$filter){
	  id
	  text
	  user{
		payload
	  }
	  createdAt
	}
  }
`

class CommentList extends React.Component {
	state = { comments: [], isLoading: true};

	componentDidMount() {
		this.props.onRef(this)
	}

	componentWillUnmount() {
        this.props.onRef(null)
    }

	addNewComment(comment) {
		var newList = this.state.comments.slice();    
		newList.push(comment);
		this.setState({comments:newList});
	}
	
	render(){
		if (!this.props.CommentsQuery || this.props.CommentsQuery.loading || !this.props.CommentsQuery.allComments) {
			return <Loading isLoading={this.state.isLoading} />
		}
		var postComments = this.props.CommentsQuery.allComments
			.map((comment) => (
				<Row key={comment.id}>
					<Col xs={6} md={4}>
					<Media>
						<Media.Left>
						<img width={64} height={64} src={JSON.parse(comment.user.payload).picture} alt={JSON.parse(comment.user.payload).name} />
						</Media.Left>
						<Media.Body>
						<OverlayTrigger placement="right" overlay={<Tooltip id="tooltip">{comment.createdAt}</Tooltip>}>
							<Media.Heading>{JSON.parse(comment.user.payload).name}</Media.Heading>
						</OverlayTrigger>
						<p>
							{comment.text}
						</p>
						</Media.Body>
					</Media>
					<br />
					</Col>
				</Row>
			))
		return (
			<div>
				<Grid>
					{postComments}
				</Grid>
			</div>
		);
	}
}

const mapPropsToOptions = props => ({ variables: {
	filter: {post: {id: props.postId}}
  } });

export default graphql(COMMENTS_QUERY, { name: 'CommentsQuery', options: mapPropsToOptions})(CommentList)