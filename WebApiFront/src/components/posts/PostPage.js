import React, { Component } from 'react';
import { ControlLabel, Col, Image, Button, Glyphicon } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import * as constants from '../Constants';
import Loading from '../Loading';
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'
import history from '../auth/History';
import CommentBox from './CommentBox';
import CommentsList from './CommentsList';
import PubSub from 'pubsub-js'

const POST_QUERY = gql`
query PostQuery($id: ID) {
    Post(id:$id){
      createdAt
      imageUrl
      title
      user{
        id
        payload
      }
    }
  }
`

const DELETE_MUTATION = gql`
mutation DeletePost($id:ID!){
    deletePost(id:$id){
      createdAt
      id
      imageUrl
      title
    }
  }
`

class PostPage extends Component {
    state = { post: '', isLoading: true };

    deletePost = () => {
        this.props.DeleteMutation({
            variables: {
                id: this.props.match.params.id
            },
        }).then((result) => {
            history.replace(constants.POSTS_ROUTE)
            PubSub.publish(constants.SUBSCRIBE_DELETE_POST, result);
        })
    }

    render() {
        if (this.props.id === '')
            return <div></div>
        if (!this.props.PostQuery || this.props.PostQuery.loading || !this.props.PostQuery.Post) {
            return <div><Link to={constants.POSTS_ROUTE}><Button bsStyle="primary">{constants.BUTTON_BACK}</Button></Link>
                <Loading isLoading={this.state.isLoading} /></div>
        }
        const post = this.props.PostQuery.Post
        return (
            <div>
                <Col xs={6} md={6}>
                    <Link to={constants.POSTS_ROUTE}><Button bsStyle="primary">{constants.BUTTON_BACK}</Button></Link>
                    <br />
                    <ControlLabel>{post.title}</ControlLabel>
                    {post.user.id === this.props.auth.getGraphCoolId() ? <Glyphicon glyph="flash" /> : null}
                    <br />
                    <Image src={post.imageUrl} />
                    <br />
                    <ControlLabel>{JSON.parse(post.user.payload).name}</ControlLabel>
                    <br />
                    <ControlLabel>{post.createdAt}</ControlLabel>
                    <br />
                    {this.props.auth.isAuthenticated() ? <CommentBox auth={this.props.auth} onNewComment={this.onNewComment} postId={this.props.match.params.id}/> : null}
                    <CommentsList postId={this.props.match.params.id} onRef={ref => this.commentsList = ref}/>
                </Col>
                <Col xs={6} md={6}>
                    {post.user.id === this.props.auth.getGraphCoolId() ? <Button bsStyle="primary" onClick={ this.deletePost }>{constants.BUTTON_DELETE}</Button> : null}
                </Col>
            </div>
        );
    }
}

const mapPropsToOptions = props => ({ variables: { id: props.match.params.id } });

export default compose(graphql(DELETE_MUTATION, { name: 'DeleteMutation' }),
graphql(POST_QUERY, { name: 'PostQuery', options: mapPropsToOptions }))(PostPage)