import React from 'react';
import { Row, Col, Image, Grid, ControlLabel, Glyphicon } from 'react-bootstrap';
import '../../css/Comments.css';
import * as constants from '../Constants';
import Loading from '../Loading';
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'
import { Link } from 'react-router-dom';
import PubSub from 'pubsub-js'

const POSTS_QUERY = gql`
query GetPosts{
	allPosts{
	  id
	  createdAt
	  imageUrl
	  title
	  user {
		id
	  }
	}
  }
`

class PostsList extends React.Component {
	state = { posts: [], isLoading: true };

	componentDidMount() {
		this.addPostToken = PubSub.subscribe(constants.SUBSCRIBE_ADD_POST, this.addPostToList);
		this.deletePostToken = PubSub.subscribe(constants.SUBSCRIBE_DELETE_POST, this.deletePostFromList);
		this.createMessageSubscription = this.props.PostsQuery.subscribeToMore({
			document: gql`
			subscription ListenPosts{
				Post{
					mutation
					node{
						id
						createdAt
						title
						imageUrl
						user{
							id
						}
					}
					previousValues{
						id
					}
				}
			}
			`,
			updateQuery: (previousState, {subscriptionData}) => {
				const newMessage = subscriptionData.data.Post.node
				let messages = []
				if (subscriptionData.data.Post.mutation === 'CREATED') {
					messages = previousState.allPosts.concat([newMessage])
				}
				if (subscriptionData.data.Post.mutation === 'DELETED') {
					messages = previousState.allPosts.filter((item) => { return item.id !== subscriptionData.data.Post.previousValues.id} );
				}
				this.setState({posts: messages});
				return {
					allPosts: messages,
				}
			},
			onError: (err) => console.error(err),
		})
	}

	componentWillUnmount(){
		PubSub.unsubscribe(this.addPostToken);
		PubSub.unsubscribe(this.deletePostToken);
	  }

	addPostToList = (msg, data) => {
		this.props.PostsQuery.refetch()
	}

	deletePostFromList = (msg, data) => {
		this.props.PostsQuery.refetch()
	}

	render() {
		if (!this.props.PostsQuery || this.props.PostsQuery.loading || !this.props.PostsQuery.allPosts) {
			return <Loading isLoading={this.state.isLoading} />
		}

		var postNodes = this.props.PostsQuery.allPosts
			.map((post) => (
				<Row key={post.id}>
					<Col xs={6} md={4}>
						<ControlLabel><Link to={constants.POSTS_ROUTE + "/" + post.id}>{post.title}</Link></ControlLabel>
						{post.user.id === this.props.auth.getGraphCoolId() ? <Glyphicon glyph="flash" /> : null}
						<Image src={post.imageUrl} responsive />
					</Col>
				</Row>))
		return (
			<div>
				<Grid>
					{postNodes}
				</Grid>
			</div>
		);
	}
}

export default graphql(POSTS_QUERY, { name: 'PostsQuery' })(PostsList)