import React, { Component } from 'react';
import { textarea, Button }  from 'react-bootstrap';
import * as constants from '../Constants';
import { graphql } from 'react-apollo'
import gql from 'graphql-tag'

const NEWCOMMENT_MUTATION = gql`
mutation NewComment($text:String!,$postId:ID!,$userId:ID!){
    createComment(text:$text,postId:$postId,userId:$userId){
      user{
        payload
      }
    }
  }
`

class CommentBox extends Component {
    state = { textBox: ''}

    addNewComment = () => {
        this.props.NewCommentMutation({
            variables: {
                text: this.state.textBox,
                postId: this.props.postId,
                userId: this.props.auth.getGraphCoolId()
            },
        }).then((result) => {
            this.setState({textBox: ''});
        })
    }

    onTextChange = (event) => {
        this.setState({textBox: event.target.value});
    }

    render() {
        return (
            <div>
            <textarea value={this.state.textBox} onChange={this.onTextChange}></textarea>
            <Button bsStyle="primary" onClick={this.addNewComment}>{constants.BUTTON_ADD}</Button>
            </div>
        );
      }
};

export default graphql(NEWCOMMENT_MUTATION, { name: 'NewCommentMutation' })(CommentBox)