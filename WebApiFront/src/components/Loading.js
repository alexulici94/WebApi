import React, { Component } from 'react';
import '../css/Loader.css'

class Loading extends Component {
    render() {
        return (
            this.props.isLoading ? <div className="loader"></div> : null
        );
      }
};

export default Loading;