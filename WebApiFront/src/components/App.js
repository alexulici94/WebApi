import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import Profile from './user/Profile';

class App extends Component {

  login() {
    this.props.auth.login();
  }

  logout() {
    this.props.auth.logout();
  }

  render() {
    const { isAuthenticated, logout } = this.props.auth;

    return (
      <div>
        {
              !isAuthenticated() && (
                  <Button
                    bsStyle="primary"
                    className="btn-margin"
                    onClick={this.login.bind(this)}
                  >
                    Log In
                  </Button>
                )
            }
            {
              isAuthenticated() && (
                <div>
                  <Profile auth={this.props.auth}/>
                  <Button
                    bsStyle="primary"
                    className="btn-margin"
                    onClick={this.logout.bind(this)}
                  >
                    Log Out
                  </Button>
                </div>
                )
            }
      </div>
    );
  }
}

export default App;
