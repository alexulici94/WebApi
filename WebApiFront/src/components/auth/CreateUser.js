import React, { Component } from 'react';
import { graphql, compose } from 'react-apollo'
import gql from 'graphql-tag'
import jwt from 'jsonwebtoken'
import * as constants from '../Constants';
import history from './History';

const getGraphcoolUser = gql`
  query getUser($auth0UserId: String!){
      User(auth0UserId: $auth0UserId){
          id,
          payload
      }
  }
`
const propsToOptionsGetGraphcoolUser = props => ({ variables: { auth0UserId: props.id } });

const createGraphCoolUser = gql`
  mutation CreateUserMutation($idToken: String!, $payload: String!){
    createUser(auth0UserId: $idToken, payload: $payload){
      id
    }
  }
`

class CreateUser extends Component {

  componentWillReceiveProps(nextProps) {
    if (nextProps.id !== '' && nextProps.GraphCoolUser && !nextProps.GraphCoolUser.loading && !nextProps.GraphCoolUser.User) {
      const decoded = jwt.decode(nextProps.auth.getIdToken(), { complete: true })
        nextProps.CreateUserMutation({
          variables: {
            idToken: decoded.payload.sub,
            payload: JSON.stringify(decoded.payload)
          },
        }).then((result) => {
          nextProps.auth.setGraphCoolId(result.data.createUser.id)
          history.replace(constants.POSTS_ROUTE)
        })
    }
    else
    {
      if(!nextProps.GraphCoolUser.loading && nextProps.GraphCoolUser.User){
        nextProps.auth.setGraphCoolId(nextProps.GraphCoolUser.User.id)
        nextProps.auth.setProfile(JSON.parse(nextProps.GraphCoolUser.User.payload));
        history.replace(constants.POSTS_ROUTE)
      }
    }
  }

    render() {
      return (
        <div>
        </div>
      );
    }
  }
  

const callBackWithDataAndMutation = compose(graphql(getGraphcoolUser, { name: 'GraphCoolUser', options: propsToOptionsGetGraphcoolUser }),
  graphql(createGraphCoolUser, { name: 'CreateUserMutation' }))(CreateUser)

  export default callBackWithDataAndMutation;