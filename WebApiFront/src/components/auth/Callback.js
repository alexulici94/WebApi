import React, { Component } from 'react';
import Loading from '../Loading';
import CreateUser from './CreateUser';

class Callback extends Component {
  state = { id: '' };

  handleAuthentication = (callback) => {
    if (/access_token|id_token|error/.test(this.props.location.hash)) {
      this.props.auth.handleAuthentication(callback);
    }
  }

  componentDidMount() {
    this.handleAuthentication(() => {
      this.setState({id:this.props.auth.getProfile().sub})
    });
  }

  render() {
    return (
      <div>
        {this.state.id !== '' ? <CreateUser id={this.state.id} auth={this.props.auth} /> : ''}
        <Loading isLoading="True" />
      </div>
    );
  }
}

export default Callback;