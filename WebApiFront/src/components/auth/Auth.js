import auth0 from 'auth0-js';
import * as constants from '../Constants';

export default class Auth {
  auth0 = new auth0.WebAuth({
    domain: constants.AUTH0_DOMAIN,
    clientID: constants.AUTH0_CLIENTID,
    redirectUri: constants.AUTH0_CALLBACK,
    audience: constants.AUTH0_IDENTIFIER,
    responseType: 'token id_token',
    scope: 'openid profile email'
  });

  userProfile;

  constructor() {
    this.login = this.login.bind(this);
    this.handleAuthentication = this.handleAuthentication.bind(this);
    this.isAuthenticated = this.isAuthenticated.bind(this);
    this.getProfile = this.getProfile.bind(this);
  }

  handleAuthentication(callback) {
    this.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        this.setSession(authResult);
      } else if (err) {
        console.log(err);
      }
      callback();
    });
  }

  setSession(authResult) {
    let expiresAt = JSON.stringify((authResult.expiresIn * 1000) + new Date().getTime());
    localStorage.setItem(constants.ACCESS_TOKEN, authResult.accessToken);
    localStorage.setItem(constants.ID_TOKEN, authResult.idToken);
    localStorage.setItem(constants.EXPIRES_AT, expiresAt);
    this.setProfile(authResult.idTokenPayload);
  }

  setProfile(profile){
    localStorage.setItem(constants.USER_PROFILE, JSON.stringify(profile));
  }

  setGraphCoolId(id){
    localStorage.setItem(constants.GRAPHCOOL_ID, id);
  }

  getGraphCoolId() {
    const graphcoolId = localStorage.getItem(constants.GRAPHCOOL_ID);
    return graphcoolId;
  }

  getAccessToken() {
    const accessToken = localStorage.getItem(constants.ACCESS_TOKEN);
    if (!accessToken) {
      throw new Error('No Access Token found');
    }
    return accessToken;
  }

  getIdToken() {
    const idToken = localStorage.getItem(constants.ID_TOKEN);
    if (!idToken) {
      throw new Error('No Id Token found');
    }
    return idToken;
  }

  getProfile() {
    const profile = JSON.parse(localStorage.getItem(constants.USER_PROFILE));
    if(profile)
      return profile;
    else
      return '';
  }

  logout() {
    this.clearLocalStorage();
    window.location.reload();
  }

  clearLocalStorage() {
    localStorage.removeItem(constants.ACCESS_TOKEN);
    localStorage.removeItem(constants.ID_TOKEN);
    localStorage.removeItem(constants.EXPIRES_AT);
    localStorage.removeItem(constants.USER_PROFILE);
    localStorage.removeItem(constants.GRAPHCOOL_ID);
  }

  isAuthenticated() {
    let response = true;
    let expiresAt = JSON.parse(localStorage.getItem(constants.EXPIRES_AT));
    if(!this.getProfile())
      response = false;
    if(new Date().getTime() > expiresAt)
      response = false;

    if(response === false)
      this.clearLocalStorage();
    
    return response;
  }

  login() {
    this.auth0.authorize();
  }
}