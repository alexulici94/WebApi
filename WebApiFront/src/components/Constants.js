//Application
export const MODALTITLE_ADD = "Add new car";
export const MODALTITLE_EDIT = "Edit car";
export const BUTTON_COMMENTS = "Comments";
export const BUTTON_CONTENTFUL= "Contentful";
export const BUTTON_POSTS = "Posts";
export const BUTTON_BACK = "Back";
export const BUTTON_CLOSE = "Close";
export const BUTTON_ADD = "Add";
export const BUTTON_DELETE = "Delete";
export const BUTTON_EDIT = "Edit";
export const POST_TITLE = "Title";
export const POST_URL = "Url";
export const MODALBODY_INPUT1_LABEL = "Id";
export const MODALBODY_INPUT2_LABEL = "Name";
export const MODALBODY_INPUT3_LABEL = "Price";
export const MODALBODY_INPUT4_LABEL = "Color";
export const ACTION_ADD = "Add";
export const ACTION_EDIT = "Edit";
export const ACTION_DELETE = "Delete";
export const VEHICLE_DETAILS_MODEL = "Model";
export const VEHICLE_DETAILS_CLASS = "Class";
export const VEHICLE_DETAILS_FILMS = "Films";
export const VEHICLE_DETAILS_CREATED_AT = "Created At";
export const NETWORK_RESPONSE_FAILED = "Network response failed";
export const CARS_API = "localhost:56823";
export const CONTENTFUL_API = "localhost:60645";

//GraphQL
export const GRAPHQL_SERVER_ADRESS = "https://api.graph.cool/simple/v1/cjdd4m40r0iom015578675uwa";
export const GRAPHQL_SUBSCRIBE_ADRESS = "wss://subscriptions.us-west-2.graph.cool/v1/cjdd4m40r0iom015578675uwa";

//Consul
export const CONSUL_SERVER_ADRESS = "http://localhost:8500/v1/kv/";
export const CONSUL_FEATURES_KEY = "features";

//Features
export const FEATURE_COMMENTS = "comments";
export const FEATURE_CONTENTFUL = "contentful";
export const FEATURE_POSTS = "posts";

//Subscribers
export const SUBSCRIBE_ADD_POST = "subscribe_add_post";
export const SUBSCRIBE_DELETE_POST = "subscribe_delete_post";
export const SUBSCRIBE_ADD_COMMENT_POST = "subscribe_add_comment_post";

//Event Store
export const EVENT_STORE_SERVER_ADRESS = "http://localhost:2113";
export const CARS_MANAGEMENT_STREAM = "cars-management-stream";

//Auth0
export const AUTH0_DOMAIN = "noobsmoke.auth0.com";
export const AUTH0_IDENTIFIER = "https://noobsmoke.auth0.com/api/v2/";
export const AUTH0_CLIENTID = "hvwEgQIYZs59Pk045Ql36aKvbId9nFiY";
export const AUTH0_CALLBACK = "https://postsapp-3bf9d.firebaseapp.com/callback";

//Local Storage
export const ACCESS_TOKEN = "access_token";
export const ID_TOKEN = "id_token";
export const EXPIRES_AT = "expires_at";
export const USER_PROFILE = "profile";
export const GRAPHCOOL_ID = "graphcool_id";

//Routes
export const APP_ROUTE = "/";
export const CALLBACK_ROUTE = "/callback";
export const HOME_ROUTE = "/home";
export const COMMENTS_ROUTE = "/comments";
export const CONTENTFUL_ROUTE = "/contentful";
export const POSTS_ROUTE = "/posts";
export const POST_ROUTE = "/posts/:id";
export const ADD_POST_ROUTE = "/posts/add";